<?php
ob_start();
session_start();
include("common/header.php");

?>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		  <?php
		  if($permessi != 0)
		  {
			  $allTickets = $mysqli->query("SELECT t.id as id, t.titolo as titolo, t.descrizione as descrizione, t.date_time as data, t.stato as idstato, c.ragionesociale as cliente, u.nome as nome, u.cognome as cognome, p.nome as progetto, st.descrizione as stato FROM myproject_ticket as t INNER JOIN myproject_clienti as c ON t.id_cliente = c.id INNER JOIN myproject_utenti as u ON t.id_utente = u.id INNER JOIN myproject_progetti as p ON t.progetto = p.id INNER JOIN myproject_stato_ticket as st ON t.stato = st.id WHERE p.id = '".$_GET['id']."' AND t.stato <> 2");
		  }
		  else
		  {
			  $allTickets = $mysqli->query("SELECT t.id as id, t.titolo as titolo, t.descrizione as descrizione, t.date_time as data, t.stato as idstato, c.ragionesociale as cliente, u.nome as nome, u.cognome as cognome, p.nome as progetto, st.descrizione as stato FROM myproject_ticket as t INNER JOIN myproject_clienti as c ON t.id_cliente = c.id INNER JOIN myproject_utenti as u ON t.id_utente = u.id INNER JOIN myproject_progetti as p ON t.progetto = p.id INNER JOIN myproject_stato_ticket as st ON t.stato = st.id");
		  }
		  while($eachTicket = $allTickets->fetch_array())
		  {
			  echo "
			  <b>Utente</b>: ".$eachTicket['nome']." ".$eachTicket['cognome']." </br>
			  <b>Cliente</b>:  ".$eachTicket['cliente']."</br>
			  <b>Progetto</b>:  ".$eachTicket['progetto']."</br>
			  <b>Titolo</b>:  ".$eachTicket['titolo']."</br>
			  <b>Descrizione</b>:
			  <br /> ".$eachTicket['descrizione']." </br>
			  <b>Data e Ora</b>: ".substr($eachTicket['data'], 8, 2)."/".substr($eachTicket['data'], 5, 2)."/".substr($eachTicket['data'], 0, 4)." </br>
			  <b>Stato</b>: ".$eachTicket['stato']." </br>
			  <br />";

			  if($permessi == 0 and $eachTicket['idstato'] < 2)
			  {
				  echo "<a href='changestatusticket.php?this=".$eachTicket['idtask']."&id=".$_GET['id']."&idstato=".$eachTicket['idstato']."' class='btn btn-success'>Cambia Stato</a>";
			  }
			  echo "

			  <hr><br /><br />
			  ";
		  }

		  ?>
		  </tbody>
		</table>



		<?php
	}
	?>
	<a href='progetti.php' class='btn btn-info'>Torna ai progetti</a>
	<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTicket">Apri un ticket</button>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso può collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addTicket" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="addticket.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di un ticket</h4>
				  </div>
				  <div class="modal-body">
						Progetto: <br />
						<select id="progetto" name="progetto" class="form-control">
							<?php
								$allCustomers = $mysqli->query("SELECT * FROM myproject_progetti WHERE cliente=".$info);
								while($eachCustomer = $allCustomers->fetch_array())
								{
									echo '<option value='.$eachCustomer['id'].'>'.$eachCustomer['nome'].'</option>';
								}
							?>
						</select><br />
						<input type="text" name="titolo" class="form-control" placeholder="Titolo*"  /> <br />
						<textarea name="descrizione" class="form-control" placeholder="Descrizione*" ></textarea> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Ticket</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
