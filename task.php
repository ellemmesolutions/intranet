<?php
ob_start();
session_start();
include("common/header.php");

?>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		  <?php
		  if($permessi != 0)
		  {
			  $allTask = $mysqli->query("SELECT u.nome as nome, u.cognome as cognome, c.ragionesociale as ragionesociale, t.descrizione as descrizione, t.termine as termine, ts.descrizione as stato, p.nome as nomeprogetto, t.stato as idstato, t.id as idtask FROM myproject_task as t INNER JOIN myproject_progetti as p ON t.id_progetto = p.id INNER JOIN myproject_clienti as c ON p.cliente = c.id INNER JOIN myproject_task_stato as ts ON t.stato = ts.id_stato INNER JOIN myproject_utenti as u ON t.id_utente = u.id WHERE p.id='".$_GET['id']."' AND u.id = ".$_SESSION['user']." AND t.stato <> 2");
		  }
		  else
		  {
			  $allTask = $mysqli->query("SELECT u.nome as nome, u.cognome as cognome, c.ragionesociale as ragionesociale, t.descrizione as descrizione, t.termine as termine, ts.descrizione as stato, p.nome as nomeprogetto, t.stato as idstato, t.id as idtask FROM myproject_task as t INNER JOIN myproject_progetti as p ON t.id_progetto = p.id INNER JOIN myproject_clienti as c ON p.cliente = c.id INNER JOIN myproject_task_stato as ts ON t.stato = ts.id_stato INNER JOIN myproject_utenti as u ON t.id_utente = u.id WHERE p.id='".$_GET['id']."'");
		  }
		  while($eachTask = $allTask->fetch_array())
		  {
			  echo "
			  <b>Utente</b>: ".$eachTask['nome']." ".$eachTask['cognome']." </br>
			  <b>Cliente</b>:  ".$eachTask['ragionesociale']."</br>
			  <b>Progetto</b>:  ".$eachTask['nomeprogetto']."</br>
			  <b>Termine</b>: ".substr($eachTask['termine'], 8, 2)."/".substr($eachTask['termine'], 5, 2)."/".substr($eachTask['termine'], 0, 4)." </br>
			  <b>Descrizione</b>:
			  <br /> ".$eachTask['descrizione']." </br>
			  <b>Stato</b>: ".$eachTask['stato']." </br>
			  <br />";

			  if($eachTask['idstato'] < 2)
			  {
				  echo "<a href='changestatus.php?this=".$eachTask['idtask']."&id=".$_GET['id']."&idstato=".$eachTask['idstato']."' class='btn btn-success'>Cambia Stato</a>";
			  }
			  echo "

			  <hr><br /><br />
			  ";
		  }

		  ?>
		  </tbody>
		</table>



		<?php
		if($permessi == 0)
		{
			?>
				<br /><br /><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addTask">Aggiungi un task</button>
			<?php
		}
	}
	?>
	<a href='progetti.php' class='btn btn-info'>Torna ai progetti</a>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso può collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addTask" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="addtask.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di un task</h4>
				  </div>
				  <div class="modal-body">
						<select id="utente" name="utente" class="form-control">
							<?php
								$allCustomers = $mysqli->query("SELECT * FROM myproject_utenti WHERE permessi < 2	");
								while($eachCustomer = $allCustomers->fetch_array())
								{
									echo '<option value='.$eachCustomer['id'].'>'.$eachCustomer['nome'].'</option>';
								}
							?>
						</select><br />
						<select id="progetto" name="progetto" class="form-control">
							<?php
								$allCustomers = $mysqli->query("SELECT * FROM myproject_progetti");
								while($eachCustomer = $allCustomers->fetch_array())
								{
									echo '<option value='.$eachCustomer['id'].'>'.$eachCustomer['nome'].'</option>';
								}
							?>
						</select><br />
						Termine Task <br />
						<input type="date" name="termine" class="form-control" /> <br />
						<textarea name="descrizione" class="form-control" placeholder="Descrizione*" ></textarea> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Task</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
