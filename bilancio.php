<?php
ob_start();
session_start();
include("common/header.php");
if(!isset($_GET["anno"]))
{
	$anno_fiscale = date("Y");
}
else
{
	$anno_fiscale = $_GET["anno"];
}

function formatta_euro($num)
{
  $eu = number_format($num, 2, ',', '.');
  return $eu . " Euro";
}
?>
	<script>
		function cambio_anno()
		{
			var nuovo_anno = document.getElementById("anno").value;
			 window.location.replace("fatturazione.php?anno=" + nuovo_anno);
		}
		function checkCustomer(cliente)
		{
			if(cliente == 2)
			{
				document.getElementById("customers").style.display = "block";
			}
			else
			{
				document.getElementById("customers").style.display = "none";
			}
		}
	</script>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		Anno Fiscale
		<select name="anno" id="anno" onchange="cambio_anno()">
			<?php
				$partenza = 2016;
				$arrivo = date("Y");
				for($i = $partenza; $i <= $arrivo; $i++)
				{
					if($anno_fiscale == $i)
					{
						echo "<option value=".$i." selected>".$i."</option>";
					}
					else
					{
						echo "<option value=".$i.">".$i."</option>";
					}
				}
			?>
		</select>
		<br /><br />
		<table class="table">
		  <thead>
			<tr>
			  <th>Mese</th>
			  <th>Anno</th>
			  <th>Descrizione</th>
			  <th>Tipologia</th>
			  <th>Importo</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php
			$folderPDF = "resources/fatture/";
			$totale_annuo = 0;
			$allBilancio = $mysqli->query("SELECT * FROM myproject_balance WHERE anno = ".$anno_fiscale." ORDER BY mese");
			while($eachBilancio = $allBilancio->fetch_array())
			{
                if($eachBilancio['type'] == 0)
                {
                    $tipologia = "Spese";
                    $segno = "- ";
                    $totale_annuo = $totale_annuo - $eachBilancio['importo'];
                }
                else
                {
                    $tipologia = "Ricavi";
                    $segno = "";
                    $totale_annuo = $totale_annuo + $eachBilancio['importo'];
                }
				echo '
					<tr>
					  <td>'.$eachBilancio['mese'].'</td>
					  <td>'.$eachBilancio['anno'].'</td>
					  <td>'.$eachBilancio['descrizione'].'</td>
					  <td>'.$tipologia.'</td>
					  <td>'.$segno.formatta_euro($eachBilancio['importo']).'</td>
					</tr>
				';
			}

		  ?>
		  </tbody>
		</table>



		<?php
		if($permessi < 2)
		{
			?>
				<br /><br /><b>Bilancio <?php echo $anno_fiscale; ?></b>: <?php echo formatta_euro($totale_annuo); ?>
				<br /><br /><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addBilancio">Nuova Voce</button>
			<?php
		}
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso pu� collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addBilancio" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="addbilancio.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di una voce di bilancio</h4>
				  </div>
				  <div class="modal-body">
                      <select name="mese">
                            <option value=1>Gennaio</option>
                            <option value=2>Febbraio</option>
                            <option value=3>Marzo</option>
                            <option value=4>Aprile</option>
                            <option value=5>Maggio</option>
                            <option value=6>Giugno</option>
                            <option value=7>Luglio</option>
                            <option value=8>Agosto</option>
                            <option value=9>Settembre</option>
                            <option value=10>Ottobre</option>
                            <option value=11>Novembre</option>
                            <option value=12>Dicembre</option>
                      </select>
                      <select name="anno">
                          <?php
                            $partenza = date("Y") - 1;
                            $arrivo = date("Y") + 3;
                            for($i = $partenza; $i < $arrivo; $i++)
                            {
                                echo '<option value='.$i.'>'.$i.'</option>';
                            }
                          ?>
                      </select>
                      <input type="text" name="descrizione" class="form-control" placeholder="Descrizione*" /> <br />
                      <select name="type">
                          <option value=0>Spesa</option>
                          <option value=1>Ricavo</option>
                      </select>
					  <input type="text" name="importo" class="form-control" placeholder="Importo (senza segni)*" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Voce</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
