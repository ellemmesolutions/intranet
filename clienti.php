<?php
ob_start();
session_start();
include("common/header.php");

?>
	<script>
		function checkCustomer(cliente)
		{
			if(cliente == 2)
			{
				document.getElementById("customers").style.display = "block";
			}
			else
			{
				document.getElementById("customers").style.display = "none";
			}
		}
	</script>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		<table class="table">
		  <thead>
			<tr>
			  <th>Rag. Soc.</th>
			  <th>Partita IVA / CF</th>
			  <th>Referente</th>
			  <th>Telefono</th>
			  <th>Indirizzo</th>
			  <th>CAP</th>
			  <th>Città</th>
			  <th>Prov</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php
			$allUsers = $mysqli->query("SELECT * FROM myproject_clienti");
			while($eachUser = $allUsers->fetch_array())
			{
				echo '
					<tr>
					  <td>'.$eachUser['ragionesociale'].'</td>
					  <td>'.$eachUser['cfiva'].'</td>
					  <td>'.$eachUser['referente'].'</td>
					  <td>'.$eachUser['telefono'].'</td>
					  <td>'.$eachUser['indirizzo'].'</td>
					  <td>'.$eachUser['cap'].'</td>
					  <td>'.$eachUser['city'].'</td>
					  <td>'.$eachUser['provincia'].'</td>
					</tr>
				';
			}

		  ?>
		  </tbody>
		</table>



		<?php
		if($permessi == 0)
		{
			?>
				<br /><br /><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addCustomer">Aggiungi un cliente</button>
			<?php
		}
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso può collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addCustomer" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="addcustomer.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di un utente</h4>
				  </div>
				  <div class="modal-body">
						<input type="text" name="ragionesociale" class="form-control" placeholder="Ragione Sociale*" /> <br />
						<input type="text" name="cfiva" class="form-control" placeholder="Partita IVA / Codice Fiscale*" /> <br />
						<input type="text" name="referente" class="form-control" placeholder="Referente*" /> <br />
						<input type="text" name="telefono" class="form-control" placeholder="Numero di telefono*" /> <br />
						<input type="mail" name="mail" class="form-control" placeholder="Email*" /> <br />
						<input type="text" name="indirizzo" class="form-control" placeholder="Indirizzo*" /> <br />
						<input type="text" name="cap" class="form-control" placeholder="CAP*" /> <br />
						<input type="text" name="city" class="form-control" placeholder="Città*" /> <br />
						<input type="text" name="provincia" class="form-control" placeholder="Provincia*" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Cliente</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
