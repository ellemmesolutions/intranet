<?php
ob_start();
session_start();
include("common/header.php");

?>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		<table class="table">
		  <thead>
			<tr>
			  <th>Data</th>
			  <th>Ora</th>
			  <th>Utente</th>
			  <th>Log</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php
			$allLogs = $mysqli->query("SELECT l.id as id, u.nome as nome, u.cognome as cognome, l.log as log, l.date_time as data FROM myproject_log as l INNER JOIN myproject_utenti as u oN l.id_utente = u.id ORDER BY l.date_time DESC");
			while($eachLog = $allLogs->fetch_array())
			{
				echo '
					<tr>
					  <td>'.substr($eachLog['data'],8,2).'/'.substr($eachLog['data'],5,2).'/'.substr($eachLog['data'],0,4).'</td>
					  <td>'.substr($eachLog['data'],11,8).'</td>
					  <td>'.$eachLog['nome'].' '.$eachLog['cognome'].'</td>
					  <td>'.$eachLog['log'].'</td>
					</tr>
				';
			}

		  ?>
		  </tbody>
		</table>



		<?php
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso può collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addUser" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="adduser.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di un utente</h4>
				  </div>
				  <div class="modal-body">
						<input type="text" name="nome" class="form-control" placeholder="Nome*" /> <br />
						<input type="text" name="cognome" class="form-control" placeholder="Cognome*" /> <br />
						<input type="mail" name="user" class="form-control" placeholder="Email*" /> <br />
						<input type="text" name="cf" class="form-control" placeholder="Codice Fiscale" /> <br />
						<input type="text" name="piva" class="form-control" placeholder="Partita IVA" /> <br />
						<select id="permessi" name="permessi" class="form-control" onchange="checkCustomer(this.value)">
							<option value=-1>Tipologia utente</option>
							<option value=0>Amministratore</option>
							<option value=1>Collaboratore</option>
							<option value=2>Cliente</option>
						</select><br />
						<div id="customers" style="display: none;">
							<select id="info" name="info" class="form-control">
								<option value=>Scegli cliente</option>
								<?php
									$allCustomers = $mysqli->query("SELECT * FROM myproject_clienti");
									while($eachCustomer = $allCustomers->fetch_array())
									{
										echo '<option value='.$eachCustomer['id'].'>'.$eachCustomer['ragionesociale'].'</option>';
									}
								?>
							</select><br />
						</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Utente</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
