<?php
ob_start();
session_start();
include("common/header.php");
if(!isset($_GET["anno"]))
{
	$anno_fiscale = date("Y");
}
else
{
	$anno_fiscale = $_GET["anno"];
}

function formatta_euro($num)
{
  $eu = number_format($num, 2, ',', '.');
  return $eu . " Euro";
}
?>
	<script>
		function cambio_anno()
		{
			var nuovo_anno = document.getElementById("anno").value;
			 window.location.replace("fatturazione.php?anno=" + nuovo_anno);
		}
		function checkCustomer(cliente)
		{
			if(cliente == 2)
			{
				document.getElementById("customers").style.display = "block";
			}
			else
			{
				document.getElementById("customers").style.display = "none";
			}
		}
	</script>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		Anno Fiscale
		<select name="anno" id="anno" onchange="cambio_anno()">
			<?php
				$partenza = 2016;
				$arrivo = date("Y");
				for($i = $partenza; $i <= $arrivo; $i++)
				{
					if($anno_fiscale == $i)
					{
						echo "<option value=".$i." selected>".$i."</option>";
					}
					else
					{
						echo "<option value=".$i.">".$i."</option>";
					}
				}
			?>
		</select>
		<br /><br />
		<table class="table">
		  <thead>
			<tr>
			  <th>Numero Fattura</th>
			  <th>Cliente</th>
			  <th>Data Fattura</th>
			  <th>Importo Netto</th>
			  <th>Cassa INPS</th>
			  <th>Bollo</th>
			  <th>Importo Lordo</th>
			  <th>Scarica</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php
			$folderPDF = "resources/fatture/";
			$totale_annuo = 0;
			$allFatturo = $mysqli->query("SELECT f.num_fattura as numero, c.ragionesociale as nomecliente, f.importo as importonetto, f.data_emissione as datafattura, f.link_pdf as pdf FROM myproject_fatturazione as f INNER JOIN myproject_clienti as c ON f.cliente = c.id WHERE data_emissione like '".$anno_fiscale."-%'");
			while($eachFatturo = $allFatturo->fetch_array())
			{
				$netto = $eachFatturo['importonetto'];
				$inps = ($netto / 100) * 4;
				$lordo = $netto + $inps;
				$bollo = 0;
				if($lordo > 77)
				{
					$bollo = 2;
					$lordo += $bollo;
				}
				$totale_annuo += $lordo;
				echo '
					<tr>
					  <td>'.$eachFatturo['numero'].'</td>
					  <td>'.$eachFatturo['nomecliente'].'</td>
					  <td>'.substr($eachFatturo['datafattura'],8,2)."/".substr($eachFatturo['datafattura'],5,2)."/".substr($eachFatturo['datafattura'],0,4).'</td>
					  <td>'.formatta_euro($netto).'</td>
					  <td>'.formatta_euro($inps).'</td>
					  <td>'.formatta_euro($bollo).'</td>
					  <td>'.formatta_euro($lordo).'</td>';
					  if($eachFatturo['pdf'] != "-")
					  {
						  echo '<td><a href="'.$folderPDF.$eachFatturo['pdf'].'"><span class="glyphicon glyphicon-download"></span></a></td>';
					  }
					  else
					  {
						  echo ' <td>File not found</td>';
					  }
					  echo'
					</tr>
				';
			}

		  ?>
		  </tbody>
		</table>



		<?php
		if($permessi < 2)
		{
			?>
				<br /><br /><b>Totale fatturato anno <?php echo $anno_fiscale; ?></b>: <?php echo formatta_euro($totale_annuo); ?>
				<br /><br /><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addFattura">Nuova Fattura</button>
			<?php
		}
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso pu� collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addFattura" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form enctype="multipart/form-data" method="post" action="addfattura.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di una fattura</h4>
				  </div>
				  <div class="modal-body">
						<input type="text" name="numerofattura" class="form-control" placeholder="Numero Fattura*" /> <br />
						<select id="cliente" name="cliente" class="form-control">
						<?php
						$customers = $mysqli->query("SELECT * FROM myproject_clienti");
						while($customer = $customers->fetch_array())
						{
							echo '<option value='.$customer['id'].'>'.$customer['ragionesociale'].'</option>';
						}
						?>
						</select><br />
						<input type="text" name="importonetto" class="form-control" placeholder="Importo Fattura (Netto)*" /> <br />
						Data Fattura<br />
						<input type="date" name="datafattura" class="form-control" /><br />
						<input type="text" name="pdf" class="form-control" placeholder="Nome PDF (Caricare a mano)*" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Fattura</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
