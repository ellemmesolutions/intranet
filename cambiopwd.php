<?php
ob_start();
session_start();
include("common/header.php");
?>
<body role="document" onload="$('#chgPassword').modal('show');">
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{
		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<center><img src="resources/images/logo.png" width="50%" /></center>
		<?php
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso può collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>

<div id="chgPassword" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="chgpwd.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Cambio password</h4>
				  </div>
                  <?php
                    if(isset($_GET['success']))
                    {
                        if($_GET['success'] == 2)
                        {
                            echo "Password cambiata correttamente";
                        }
                        else if($_GET['success'] == 1)
                        {
                            echo "Errore durante l'aggiornamento! Riprovare";
                        }
                        else if($_GET['success'] == 3)
                        {
                            echo "Le due password non coincidevano, riprova!";
                        }
                        else
                        {
                            echo "La password inserita non &egrave; corretta, riprova!";
                        }
                    }
                  ?>
				  <div class="modal-body">
						<input type="password" name="now" class="form-control" placeholder="Password Attuale" /> <br />
						<input type="password" name="new" class="form-control" placeholder="Nuova Password" /> <br />
						<input type="password" name="conf_new" class="form-control" placeholder="Conferma Nuova Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Cambia la tua password</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>

    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
