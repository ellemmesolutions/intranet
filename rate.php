<?php
ob_start();
session_start();
include("common/header.php");

function formatta_euro($num)
{
  $eu = number_format($num, 2, ',', '.');
  return $eu . " Euro";
}
$anno = date("Y");
$mese = date("m");
if($mese == 12)
{
	$next = $anno + 1;
	$prossimo = $next."-01";
}
else
{
	$next = $mese + 1;
	$prossimo = $anno."-".$next;
}
?>
	<script>
		function scadute()
		{
			document.getElementById('scadute').style.display = 'block';
			document.getElementById('imminenti').style.display = 'none';
			document.getElementById('future').style.display = 'none';
			tabScadute.className = "active";
			tabImminenti.className = "";
			tabFuture.className = "";
		}
		function imminenti()
		{
			document.getElementById('scadute').style.display = 'none';
			document.getElementById('imminenti').style.display = 'block';
			document.getElementById('future').style.display = 'none';
			tabScadute.className = "";
			tabImminenti.className = "active";
			tabFuture.className = "";
		}
		function future()
		{
			document.getElementById('scadute').style.display = 'none';
			document.getElementById('imminenti').style.display = 'none';
			document.getElementById('future').style.display = 'block';
			tabScadute.className = "";
			tabImminenti.className = "";
			tabFuture.className = "active";
		}
	</script>
    <div class="container theme-showcase" role="main">

	
	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{
		
		?>
		
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		<ul class="nav nav-tabs">
		  <li id="tabScadute"><a href="#" onclick="scadute()">Scadute</a></li>
		  <li id="tabImminenti" class="active"><a href="#" onclick="imminenti()">Imminenti</a></li>
		  <li id="tabFuture"><a href="#" onclick="future()">Future</a></li>
		</ul>
		
		<div id="scadute" style="display: none;">
			<?php
				$query = "SELECT r.id as id, r.scadenza as scadenza, c.ragionesociale as cliente, r.importo as importo, r.pagata as pagata FROM myproject_rate as r INNER JOIN myproject_clienti as c ON r.id_cliente = c.id WHERE r.scadenza < '".$anno."-".$mese."-01'";
			?>
			<table class="table">
				  <thead>
					<tr>
					  <th>Cliente</th>
					  <th>Data Scadenza</th>
					  <th>Importo</th>
					  <th>Stato</th>
					</tr>
				  </thead>
				  <tbody>
				  <?php
					$allRata = $mysqli->query($query);
					while($eachRata = $allRata->fetch_array())
					{
						$label;
						if($eachRata['pagata'] == 0)
						{
							$label = '<a href="pagarata.php?id='.$eachRata['id'].'"><span class="label label-danger">Insoluta</span></a>';
						}
						else
						{
							$label = '<span class="label label-success">Pagata</span>';
						}
						echo '				
							<tr>
							  <td>'.$eachRata['cliente'].'</td>
							  <td>'.substr($eachRata['scadenza'],8,2)."/".substr($eachRata['scadenza'],5,2)."/".substr($eachRata['scadenza'],0,4).'</td>
							  <td>'.formatta_euro($eachRata['importo']).'</td>
							  <td>'.$label.'</td>
							</tr>
						';
					}
				  ?>
				  </tbody>
				</table>
		</div>
		<div id="imminenti">
			<?php
				$query = "SELECT r.id as id, r.scadenza as scadenza, c.ragionesociale as cliente, r.importo as importo, r.pagata as pagata FROM myproject_rate as r INNER JOIN myproject_clienti as c ON r.id_cliente = c.id WHERE r.scadenza < '".$prossimo."-01' AND r.scadenza > '".$anno."-".$mese."-01'";
			?>
			<table class="table">
				  <thead>
					<tr>
					  <th>Cliente</th>
					  <th>Data Scadenza</th>
					  <th>Importo</th>
					  <th>Stato</th>
					</tr>
				  </thead>
				  <tbody>
				  <?php
					$allRata = $mysqli->query($query);
					while($eachRata = $allRata->fetch_array())
					{
						$label;
						if($eachRata['pagata'] == 0)
						{
							$label = '<a href="pagarata.php?id='.$eachRata['id'].'"><span class="label label-warning">Imminente</span></a>';
						}
						else
						{
							$label = '<span class="label label-success">Pagata</span>';
						}
						echo '				
							<tr>
							  <td>'.$eachRata['cliente'].'</td>
							  <td>'.substr($eachRata['scadenza'],8,2)."/".substr($eachRata['scadenza'],5,2)."/".substr($eachRata['scadenza'],0,4).'</td>
							  <td>'.formatta_euro($eachRata['importo']).'</td>
							  <td>'.$label.'</td>
							</tr>
						';
					}
				  ?>
				  </tbody>
				</table>
		</div>
		<div id="future" style="display: none;">
			<?php
				$query = "SELECT r.id as id, r.scadenza as scadenza, c.ragionesociale as cliente, r.importo as importo, r.pagata as pagata FROM myproject_rate as r INNER JOIN myproject_clienti as c ON r.id_cliente = c.id WHERE r.scadenza > '".$prossimo."-01'";
			?>
			<table class="table">
				  <thead>
					<tr>
					  <th>Cliente</th>
					  <th>Data Scadenza</th>
					  <th>Importo</th>
					  <th>Stato</th>
					</tr>
				  </thead>
				  <tbody>
				  <?php
					$allRata = $mysqli->query($query);
					while($eachRata = $allRata->fetch_array())
					{
						$label;
						if($eachRata['pagata'] == 0)
						{
							$label = '<a href="pagarata.php?id='.$eachRata['id'].'"><span class="label label-info">Da Pagare</span></a>';
						}
						else
						{
							$label = '<span class="label label-success">Pagata</span>';
						}
						echo '				
							<tr>
							  <td>'.$eachRata['cliente'].'</td>
							  <td>'.substr($eachRata['scadenza'],8,2)."/".substr($eachRata['scadenza'],5,2)."/".substr($eachRata['scadenza'],0,4).'</td>
							  <td>'.formatta_euro($eachRata['importo']).'</td>
							  <td>'.$label.'</td>
							</tr>
						';
					}
				  ?>
				  </tbody>
				</table>
		</div>
		
		<?php
		if($permessi < 2)
		{
			?>
				<br /><br /><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addRata">Nuova Rata</button>
			<?php
		}
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso pu� collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>
			
			
			
			<div id="addRata" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form enctype="multipart/form-data" method="post" action="addrata.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di una rata</h4>
				  </div>
				  <div class="modal-body">
						<input type="text" name="numerorata" class="form-control" placeholder="Numero Rata*" /> <br />
						<select id="cliente" name="cliente" class="form-control">
						<?php
						$customers = $mysqli->query("SELECT * FROM myproject_clienti");
						while($customer = $customers->fetch_array())
						{
							echo '<option value='.$customer['id'].'>'.$customer['ragionesociale'].'</option>';
						}
						?>
						</select><br />
						<input type="text" name="importo" class="form-control" placeholder="Importo Rata*" /> <br />
						Data Scadenza<br />
						<input type="date" name="scadenza" class="form-control" /><br />
						<textarea name="note" class="form-control" placeholder="Eventuali note"> </textarea><br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Rata</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
