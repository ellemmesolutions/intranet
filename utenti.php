<?php
ob_start();
session_start();
include("common/header.php");

?>
	<script>
		function checkCustomer(cliente)
		{
			if(cliente == 2)
			{
				document.getElementById("customers").style.display = "block";
			}
			else
			{
				document.getElementById("customers").style.display = "none";
			}
		}
	</script>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		<table class="table">
		  <thead>
			<tr>
			  <th>Nome</th>
			  <th>Cognome</th>
			  <th>Username</th>
			  <th>Codice Fiscale</th>
			  <th>Permessi</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php
			$allUsers = $mysqli->query("SELECT * FROM myproject_utenti");
			while($eachUser = $allUsers->fetch_array())
			{
				switch($eachUser['permessi'])
				{
					case 0:
						$strpermessithis = "AMMINISTRATORE DI SISTEMA";
						break;
					case 1:
						$strpermessithis = "COLLABORATORE";
						break;
					case 2:
						$strpermessithis = "CLIENTE";
						break;
				}
				echo '
					<tr>
					  <td>'.$eachUser['nome'].'</td>
					  <td>'.$eachUser['cognome'].'</td>
					  <td>'.$eachUser['mail'].'</td>
					  <td>'.$eachUser['codicefiscale'].'</td>
					  <td>'.$strpermessithis.'</td>
					</tr>
				';
			}

		  ?>
		  </tbody>
		</table>



		<?php
		if($permessi == 0)
		{
			?>
				<br /><br /><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser">Aggiungi un utente</button>
			<?php
		}
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso può collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addUser" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="adduser.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di un utente</h4>
				  </div>
				  <div class="modal-body">
						<input type="text" name="nome" class="form-control" placeholder="Nome*" /> <br />
						<input type="text" name="cognome" class="form-control" placeholder="Cognome*" /> <br />
						<input type="mail" name="user" class="form-control" placeholder="Email*" /> <br />
						<input type="text" name="cf" class="form-control" placeholder="Codice Fiscale" /> <br />
						<input type="text" name="piva" class="form-control" placeholder="Partita IVA" /> <br />
						<select id="permessi" name="permessi" class="form-control" onchange="checkCustomer(this.value)">
							<option value=-1>Tipologia utente</option>
							<option value=0>Amministratore</option>
							<option value=1>Collaboratore</option>
							<option value=2>Cliente</option>
						</select><br />
						<div id="customers" style="display: none;">
							<select id="info" name="info" class="form-control">
								<option value=>Scegli cliente</option>
								<?php
									$allCustomers = $mysqli->query("SELECT * FROM myproject_clienti");
									while($eachCustomer = $allCustomers->fetch_array())
									{
										echo '<option value='.$eachCustomer['id'].'>'.$eachCustomer['ragionesociale'].'</option>';
									}
								?>
							</select><br />
						</div>
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Utente</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
