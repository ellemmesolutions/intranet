<?php
ob_start();
session_start();
include("common/header.php");

?>
	<script>
		function checkCustomer(cliente)
		{
			if(cliente == 2)
			{
				document.getElementById("customers").style.display = "block";
			}
			else
			{
				document.getElementById("customers").style.display = "none";
			}
		}
	</script>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		<table class="table">
		  <thead>
			<tr>
			  <th>Progetto</th>
			  <th>Ore Lavorate</th>
			  <th>Descrizione</th>
			  <th>Stato</th>
			</tr>
		  </thead>
		  <tbody>
		  <?php
			$allConsuntivo = $mysqli->query("SELECT p.nome as nome, c.ore as ore, c.descrizione as descrizione, s.descrizione as descrizionestato, s.simbolo as simbolo FROM myproject_consuntivo as c INNER JOIN myproject_progetti as p ON p.id = c.id_progetto INNER JOIN myproject_stato_consuntivazione as s ON s.id = c.stato WHERE c.id_utente = ".$_SESSION['user']);
			while($eachConsuntivo = $allConsuntivo->fetch_array())
			{
				echo '
					<tr>
					  <td>'.$eachConsuntivo['nome'].'</td>
					  <td>'.$eachConsuntivo['ore'].'</td>
					  <td>'.$eachConsuntivo['descrizione'].'</td>
					  <td><span class="'.$eachConsuntivo['simbolo'].'"></span></td>
					</tr>
				';
			}

		  ?>
		  </tbody>
		</table>



		<?php
		if($permessi < 2)
		{
			?>
				<br /><br /><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addConsuntivo">Consuntiva</button>
			<?php
		}
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso può collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addConsuntivo" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="addconsuntivo.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di una consuntivazione</h4>
				  </div>
				  <div class="modal-body">
						<select id="info" name="progetto" class="form-control">
						<?php
						$projects = $mysqli->query("SELECT * FROM myproject_progetti");
						while($project = $projects->fetch_array())
						{
							$myColl = explode(";", $project["collaboratori"]);
							foreach($myColl as $coll)
							{
								if($coll == $_SESSION['user'])
								{
									echo '<option value='.$project['id'].'>'.$project['nome'].'</option>';
								}
							}
						}
						?>
						</select><br />
						<input type="text" name="ore" class="form-control" placeholder="Ore (in centesimi)*" /> <br />
						<textarea name="descrizione" class="form-control" placeholder="Descrizione del consuntivo*" ></textarea> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Consuntivazione</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
