<?php
ob_start();
session_start();
include("common/header.php");

?>
	<script>
		function checkCustomer(cliente)
		{
			if(cliente == 2)
			{
				document.getElementById("customers").style.display = "block";
			}
			else
			{
				document.getElementById("customers").style.display = "none";
			}
		}
	</script>
    <div class="container theme-showcase" role="main">


	<?php
	if(isset($_SESSION['user']) and $_SESSION['user'] != "")
	{

		?>
		Benvenuto, <?php echo $nome." ".$cognome.". Il tuo ruolo &egrave; <b>".$strpermessi."</b>"; ?>
		<br /><br /><br /><br />
		<table class="table">
		  <thead>
			<tr>
			  <th>Nome Progetto</th>
			  <th>Tipologia</th>
			  <th>Cliente</th>
			  <th>Data Inizio</th>
			  <th>Termine Consegna</th>
		  <?php
		  if($permessi == 2)
		  {
			  echo "

			  <th>Ticket</th>
			</tr>
		  </thead>
		  <tbody>
			  ";
			$projectsCustomer = $mysqli->query("SELECT * FROM myproject_progetti WHERE cliente = ".$info);
			while($projectCustomer = $projectsCustomer->fetch_array())
			{
				$customers = $mysqli->query("SELECT * FROM myproject_clienti WHERE id = ".$projectCustomer['cliente']);
				$customer = $customers->fetch_array();
				$datainizio = substr($projectCustomer['data_inizio'], 8, 2)."/".substr($projectCustomer['data_inizio'], 5, 2)."/".substr($projectCustomer['data_inizio'], 0, 4);
				$datafine = substr($projectCustomer['data_consegna'], 8, 2)."/".substr($projectCustomer['data_consegna'], 5, 2)."/".substr($projectCustomer['data_consegna'], 0, 4);
				echo '
					<tr>
					  <td>'.$projectCustomer['nome'].'</td>
					  <td>'.$projectCustomer['tipologia'].'</td>
					  <td>'.$customer['ragionesociale'].'</td>
					  <td>'.$datainizio.'</td>
					  <td>'.$datafine.'</td>
					  <td><a href="ticket.php?id='.$projectCustomer['id'].'"><img src="common/img/action.png" /></a></td>
					</tr>
				';
			}
		  }
		  else
		  {
			  echo "

			  <th>Task</th>
			</tr>
		  </thead>
		  <tbody>
			  ";
			$projects = $mysqli->query("SELECT * FROM myproject_progetti");
			while($project = $projects->fetch_array())
			{
				$myColl = explode(";", $project["collaboratori"]);
				foreach($myColl as $coll)
				{
					if($coll == $_SESSION['user'])
					{
						$customers = $mysqli->query("SELECT * FROM myproject_clienti WHERE id = ".$project['cliente']);
						$customer = $customers->fetch_array();
						$datainizio = substr($project['data_inizio'], 8, 2)."/".substr($project['data_inizio'], 5, 2)."/".substr($project['data_inizio'], 0, 4);
						$datafine = substr($project['data_consegna'], 8, 2)."/".substr($project['data_consegna'], 5, 2)."/".substr($project['data_consegna'], 0, 4);
						echo '
							<tr>
							  <td>'.$project['nome'].'</td>
							  <td>'.$project['tipologia'].'</td>
							  <td>'.$customer['ragionesociale'].'</td>
							  <td>'.$datainizio.'</td>
							  <td>'.$datafine.'</td>
							  <td><a href="task.php?id='.$project['id'].'"><img src="common/img/action.png" /></a></td>
							</tr>
						';
					}
				}

			}
		  }


		  ?>
		  </tbody>
		</table>



		<?php
		if($permessi == 0)
		{
			?>
				<br /><br /><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addProject">Aggiungi un progetto</button>
			<?php
		}
	}
	?>
    </div> <!-- /container -->
<div id="login" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="login.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Effettua il login</h4>
					Solo chi ha ricevuto la mail di Luca Mascherone con i dati di accesso può collegarsi al sito
				  </div>
				  <div class="modal-body">
						<input type="mail" name="user" class="form-control" placeholder="Email" /> <br />
						<input type="password" name="pwd" class="form-control" placeholder="Password" /> <br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Login</button>
				  </div>
				</div>
			</form>
			  </div>
			</div>



			<div id="addProject" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
        <form method="post" action="addproject.php">
			<div class="modal-content">
				  <div class="modal-header">
					<h4 class="modal-title">Aggiunta di un progetto</h4>
				  </div>
				  <div class="modal-body">
						<input type="text" name="nome" class="form-control" placeholder="Nome*" /> <br />
						<input type="text" name="tipologia" class="form-control" placeholder="Tipologia*" /> <br />
						<textarea name="descrizione" class="form-control" placeholder="Descrizione del progetto*" ></textarea> <br />
						<?php
								$allCollaborators = $mysqli->query("SELECT * FROM myproject_utenti WHERE permessi < 2");
								while($eachCollaborator = $allCollaborators->fetch_array())
								{
									echo '<input type="checkbox" name="col'.$eachCollaborator['id'].'" /> '.$eachCollaborator['nome']." ".$eachCollaborator['cognome'];
								}
							?>
						<select id="info" name="cliente" class="form-control">
							<option value=>Scegli cliente</option>
							<?php
								$allCustomers = $mysqli->query("SELECT * FROM myproject_clienti");
								while($eachCustomer = $allCustomers->fetch_array())
								{
									echo '<option value='.$eachCustomer['id'].'>'.$eachCustomer['ragionesociale'].'</option>';
								}
							?>
						</select><br />
						Data Inizio<br />
						<input type="date" name="data_inizio" class="form-control" /><br />
						Termine previsto<br />
						<input type="date" name="data_fine" class="form-control" /><br />
				  </div>
				  <div class="modal-footer">
					<button type="submit" class="btn btn-success">Aggiungi Progetto</button>
				  </div>
				</div>

			  </div>
			</div>

		</form>
    <!-- Bootstrap core JavaScript
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.14/angular.min.js"></script>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="dist/js/bootstrap.min.js"></script>
    <script src="assets/js/docs.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
